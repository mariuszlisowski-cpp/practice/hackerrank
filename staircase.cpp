#include <iostream>
#include <string>

void staircase(int n) {
	int m{};
	while (n) {
		std::cout << std::string(--n, ' ').append(std::string(++m, '#')) << std::endl;
	}
}

int main() {
	const int n = 5;
	staircase(n);

	return 0;
}
