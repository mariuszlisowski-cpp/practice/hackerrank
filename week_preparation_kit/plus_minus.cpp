#include <iomanip>
#include <iostream>
#include <vector>

void plusMinus(std::vector<int> arr) {
    auto denominator = arr.size();
    auto positives{0u};
    auto negatives{0u};
    auto zeroes{0u};

    for (auto el : arr) {
        if (el > 0) {
            ++positives;
        } else if (el < 0) {
            ++negatives;
        } else {
            ++zeroes;
        }
    }

    std::cout << std::fixed << std::setprecision(6)
              << positives / (denominator + .0) << std::endl
              << negatives / (denominator + .0) << std::endl
              << zeroes / (denominator + .0) << std::endl;
}


int main() {
    std::vector<int> arr{-4, 3, -9, 0, 4, 1};
    plusMinus(arr);

    return 0;
}
