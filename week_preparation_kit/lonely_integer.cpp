#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>

template <typename T>
void show_map(const T& map) {
    for (auto [key, value] : map) {
        std::cout << key << ":" << value << std::endl;
    }
}

int lonely_integer(std::vector<int> arr) {
    std::unordered_map<int, int> integers;
    // count occurences of integers
    for (auto el : arr) {
        ++integers[el];
    }
    // verbose output
    show_map(integers);
    // comare value of a map
    return std::min_element(integers.begin(), integers.end(),
                            [](auto& lhs, auto& rhs) {
                                return lhs.second < rhs.second;
                            })->first;
}

int main() {
    std::vector<int> arr{1, 2, 3, 4, 3, 2, 1};  // 4 has no duplicates

    auto lonely = lonely_integer(arr);
    std::cout << lonely << " is a lonely integer" << std::endl;

    return 0;
}
