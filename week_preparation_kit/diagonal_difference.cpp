#include <iostream>
#include <vector>

int diagonalDifference(std::vector<std::vector<int>> arr) {
    auto left_right_diagonal{0};
    for (int i = 0, j = 2; i < arr.size(); i++, j--) {
        left_right_diagonal += arr[i][i];
    }
    // verbose
    std::cout << "Diagonal sum left-right: " << left_right_diagonal << std::endl;

    auto right_left_diagonal{0};
    for (int i = arr.size() - 1, j = 0; i >= 0; i--, j++) {
        right_left_diagonal += arr[i][j];
    }
    // verbose
    std::cout << "Diagonal sum right-left: " << right_left_diagonal << std::endl;

    return std::abs(right_left_diagonal - left_right_diagonal);
}

int main() {
    std::vector<std::vector<int>> matrix{
        {1, 2, 3},
        {4, 5, 6},
        {9, 8, 9}
    };

    auto result = diagonalDifference(matrix);
    std::cout << "Diagonal difference: " << result << std::endl;

    return 0;
}
