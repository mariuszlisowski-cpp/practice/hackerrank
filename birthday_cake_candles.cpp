#include <iostream>
#include <limits>
#include <vector>

int birthdayCakeCandles(std::vector<int> candles) {
    int count{};
    for (int max{std::numeric_limits<int>::min()}; auto candle : candles) {
        if (candle > max) {
            max = candle;
            count = 1;
        } else if (candle == max) {
            max = candle;
            ++count;
        }
    }
    
    return count;
}

int main() {
    std::vector<int> candles{2, 4, 4, 1, 3, 4}; // tallest candles: 3
    std::cout << birthdayCakeCandles(candles) << std::endl;

    return 0;
}
