// linked list cycle detection
// hackerrank.com

#include <iostream>

class listNode {
public:
    int value;
    listNode* next;
};

// SOLUTION START
bool has_cycle(listNode* head) {
    if (!head) {
        return false;
    }
    listNode* fast = head->next;
    listNode* slow = head;  // avoiding collision at start
    while (fast && fast->next && slow) {
        if (fast == slow) {
            return true;
        }
        fast = fast->next->next;  // moves two steps
        slow = slow->next;        // moves one step
    }
    return false;
}
// SOLUTION END

listNode* insert_at_head(listNode* head, int data) {
    return new listNode{ data, head };
}

listNode* linkTailToHead(listNode* head) {
    listNode* temp = head;
    while (temp->next) {
        temp = temp->next;
    }
    temp->next = head;
    return head;
}

void displayList(listNode* head) {
    while (head) {
        std::cout << head->value << " -> ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    listNode* head = nullptr;

    head = insert_at_head(head, 10);
    head = insert_at_head(head, 20);
    head = insert_at_head(head, 30);

    // make a cycle
    head = linkTailToHead(head);

    std::cout << (has_cycle(head) ? "Has a cycle" : "Has NOT a cycle") << '\n';

    if (!has_cycle(head)) {
        displayList(head);
    }

    return 0;
}
