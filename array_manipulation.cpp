// array manipulation
// hackerrank.com
// YT sourced: JAVAAID "Array Manipulation Hackerrank Solution"

#include <algorithm>
#include <array>
#include <iostream>
#include <list>
#include <vector>

using namespace std;

template <class T>
void displayList(const T& ls) {
    for (const auto& e : ls)
        cout << e << " ";
    cout << endl;
}

long arrayManipulation(int n, vector<vector<int>> queries) {
    // avoiding out of bounds exception
    vector<long> v(n + 2);  // size + 2

    // verbose
    displayList(v);

    size_t size = queries.size();
    // time complexity O(m) (querries)
    for (size_t i = 0; i < size; ++i) {
        int a = queries[i].at(0);
        int b = queries[i].at(1);
        int k = queries[i].at(2);
        v.at(a) += k;
        v.at(b + 1) -= k;

        // verbose
        displayList(v);
    }
    // prefix sum
    for (int i = 1; i < v.size(); ++i)
        v[i] += v[i - 1];

    // verbose
    displayList(v);
    cout << "Max: ";

    return *max_element(v.begin(), v.end());
}

int main() {
    int size1 = 5;
    vector<vector<int>> vv1{
        { 1, 2, 100 },
        { 2, 5, 100 },
        { 3, 4, 100 }
    };
    cout << arrayManipulation(size1, vv1) << '\n';

    int size2 = 10;
    vector<vector<int>> vv2{
        { 1, 5, 3 },
        { 4, 8, 7 },
        { 6, 9, 1 }
    };
    cout << arrayManipulation(size2, vv2) << '\n';

    return 0;
}