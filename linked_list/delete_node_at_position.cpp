// delete node at position

#include <iostream>

using namespace std;

class SinglyLinkedListNode {
public:
    int data;
    SinglyLinkedListNode* next;
    SinglyLinkedListNode(int node_data) {
        this->data = node_data;
        this->next = nullptr;
    }
};

SinglyLinkedListNode* deleteNode(SinglyLinkedListNode* head, int position) {
    if (position == 0) {
        SinglyLinkedListNode* temp = head;
        head = head->next;
        delete temp;
    } else {
        int counter{};
        SinglyLinkedListNode* prev = head;
        SinglyLinkedListNode* toDel = head->next;
        while (counter < position - 1) {
            prev = prev->next;
            toDel = prev->next;
            ++counter;
        }
        prev->next = toDel->next;
        delete toDel;
    }
    return head;
}

SinglyLinkedListNode* insertNodeAtTail(SinglyLinkedListNode* head, int data) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    if (!head)
        head = node;
    else {
        SinglyLinkedListNode* last = head;
        while (last->next)
            last = last->next;
        last->next = node;
    }
    return head;
}

void displayNodes(SinglyLinkedListNode* head) {
    while (head) {
        cout << head->data << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

int main() {
    SinglyLinkedListNode* head{};

    head = insertNodeAtTail(head, 10);
    head = insertNodeAtTail(head, 20);
    head = insertNodeAtTail(head, 30);
    displayNodes(head);

    head = deleteNode(head, 0);
    displayNodes(head);

    head = deleteNode(head, 1);
    displayNodes(head);

    return 0;
}
