// insert node at tail

#include <iostream>

using namespace std;

class SinglyLinkedListNode {
public:
    int data;
    SinglyLinkedListNode* next;
    SinglyLinkedListNode(int node_data) {
        this->data = node_data;
        this->next = nullptr;
    }
};
////////////////////////////////////////////////////////////////////////////////
// SOLUTION
int getNode(SinglyLinkedListNode* head, int positionFromTail) {
    SinglyLinkedListNode* curr = head;
    size_t counter{};
    while (curr) {
        ++counter;
        curr = curr->next;
    }
    curr = head;
    for (size_t i = 1; i < counter - positionFromTail; ++i) {
        curr = curr->next;
    }

    return curr->data;
}
////////////////////////////////////////////////////////////////////////////////

SinglyLinkedListNode* insertNodeAtTail(SinglyLinkedListNode* head, int data) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    if (!head)
        head = node;
    else {
        SinglyLinkedListNode* last = head;
        while (last->next)
            last = last->next;
        last->next = node;
    }
    return head;
}

void displayNodes(SinglyLinkedListNode* head) {
    while (head) {
        cout << head->data << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

int main() {
    SinglyLinkedListNode* head{};

    head = insertNodeAtTail(head, 10);
    head = insertNodeAtTail(head, 20);
    head = insertNodeAtTail(head, 30);
    head = insertNodeAtTail(head, 40);
    head = insertNodeAtTail(head, 50);
    displayNodes(head);

    std::cout << getNode(head, 4) << '\n';

    return 0;
}
