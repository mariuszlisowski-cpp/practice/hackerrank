// singly linked list node
// ~ insert a node at Head
// ~ insert a node at Tail
// ~ insert a node at position
// ~ delete a node at position
// ~ reverse nodes
// ~ display list
// ~ display list reversed
// Head -> node -> Tail

#include <iostream>

using namespace std;

class SinglyLinkedListNode {
public:
    int data;
    SinglyLinkedListNode* next;
    SinglyLinkedListNode(int node_data = 0) {
        this->data = node_data;
        this->next = nullptr;
    }
};

// 'head' passed by reference
void insertNodeAtHead(SinglyLinkedListNode* &head, int data) {
    SinglyLinkedListNode * node = new SinglyLinkedListNode(data);
    if (!head)
        head = node;
    else {
        node->next = head;
        head = node;
    }
}

// 'head' passed by reference
void insertNodeAtTail(SinglyLinkedListNode* &head, int data) {
    SinglyLinkedListNode * node = new SinglyLinkedListNode(data);
    if (!head)
        head = node;
    else {
        SinglyLinkedListNode * last = head;
        while (last->next)
            last = last->next;
        last->next = node;
    }
}

// 'head' passed by reference
void insertNodeAtPosition(SinglyLinkedListNode* &head, int data, int position) {
    SinglyLinkedListNode * node = new SinglyLinkedListNode(data);
    if (!head) {
        head = node;
    }
    else if (position == 0) {
        node->next = head;
        head = node;
    }
    else if (position > 0) {
        // checking max allowed position
        int counter {}, maxPositon {};
        SinglyLinkedListNode * temp = head;
        while (temp) {
            temp = temp->next;
            ++maxPositon;
        }
        // within the size of the list (<=)
        if (position <= maxPositon) {
            SinglyLinkedListNode * prev = head;
            SinglyLinkedListNode * next = head->next;
            while (counter < position - 1) {
                prev = prev->next;
                next = prev->next;
                ++counter;
            }
            prev->next = node;
            node->next = next;
        }
        // position value too high
        else {
            cout << "Position non existent. Nothing done!" << endl;
        }
    }
}

// 'head' passed by reference
void deleteNodeAtPosition(SinglyLinkedListNode* &head, int position) {
    if (position == 0) {
        SinglyLinkedListNode* temp = head;
        head = head->next;
        delete temp;
    }
    else  if (position > 0) {
        // checking max allowed position
        int counter {}, maxPositon {};
        SinglyLinkedListNode * temp = head;
        while (temp) {
            temp = temp->next;
            ++maxPositon;
        }
        // within the size of the list (<)
        if (position < maxPositon) {
            SinglyLinkedListNode* prev = head;
            SinglyLinkedListNode* toDel = head->next;
            while (counter < position - 1) {
                prev = prev->next;
                toDel = prev->next;
                ++counter;
            }
            prev->next = toDel->next;
            delete toDel;
        }
        // position value too high
        else {
            cout << "Position non existent. Nothing done!" << endl;
        }
    }
}

// 'head' passed by reference
void reverseNodes(SinglyLinkedListNode* &head) {
    SinglyLinkedListNode* curr = head;
    SinglyLinkedListNode *prev {}, *next {};
    while (curr != NULL) {
        next = curr->next;
        curr->next = prev;
        prev = curr;
        curr = next;
    }
    head = prev;
}

// 'head' passed by value
void displaySinglyLinkedList(SinglyLinkedListNode * head) {
    while (head) {
        cout << head->data << " -> ";
        head = head->next; // copy of 'head' thus no change
    }
    cout << "nullptr" << endl;
}

// 'head' passed by value
void displaySinglyLinkedListReverse(SinglyLinkedListNode* head) {
    // base case to exit recursion (nullptr)
    if (!head)
        return;
    if (!head->next)
        cout << "nullptr"; // condition can be omitted
    displaySinglyLinkedListReverse(head->next);
    // returning form recursion
    cout <<  " <- " << head->data;
}

void showHead(SinglyLinkedListNode*);

int main() {
    SinglyLinkedListNode * head {};

    cout << "Insert node at tail..." << endl;
    insertNodeAtTail(head, 10);
    insertNodeAtTail(head, 20);
    displaySinglyLinkedList(head);
    showHead(head);

    cout << "Insert node at head..." << endl;
    insertNodeAtHead(head, 30);
    insertNodeAtHead(head, 40);
    displaySinglyLinkedList(head);
    showHead(head);

    cout << "Insert node at position..." << endl;
    insertNodeAtPosition(head, 70, 0);
    insertNodeAtPosition(head, 80, 5);
    displaySinglyLinkedList(head);
    showHead(head);

    cout << "Delete node at position..." << endl;
    deleteNodeAtPosition(head, 0);
    deleteNodeAtPosition(head, 4);
    displaySinglyLinkedList(head);
    showHead(head);

    cout << "Reverse nodes..." << endl;
    reverseNodes(head);
    displaySinglyLinkedList(head);
    showHead(head);

    cout << "Reversed display..." << endl;
    displaySinglyLinkedListReverse(head);
    // cout << endl;

    return 0;
}

void showHead(SinglyLinkedListNode* head) {
    cout << "Head: " << head->data << endl << endl;
}
