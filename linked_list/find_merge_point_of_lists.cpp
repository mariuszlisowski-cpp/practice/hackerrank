// find merge point of two lists

// merge two singly linked lists

#include <iostream>

class SinglyLinkedListNode {
public:
    int data;
    SinglyLinkedListNode* next;
    
    SinglyLinkedListNode() {}
    SinglyLinkedListNode(int node_data) {
        data = node_data;
        next = nullptr;
    }
};

SinglyLinkedListNode* insertNodeAtTail(SinglyLinkedListNode* head, int data) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    if (!head) {
        head = node;
    }
    else {
        SinglyLinkedListNode* last = head;
        while (last->next)
            last = last->next;
        last->next = node;
    }

    return head;
}

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
// Create two of these, pointing to two heads. Advance each of the pointers by
// 1 every time, until they meet. This will happen after either one or two passes.
// we have 2 lists: a-b-c-x-y-z and p-q-x-y-z 
// path of first pointer  a,b,c,x,y,z,p,q,x
// path of second pointer p,q,x,y,z,a,b,c,x
int findMergeNode(SinglyLinkedListNode* head1, SinglyLinkedListNode* head2) {
    SinglyLinkedListNode* curr1 = head1;
    SinglyLinkedListNode* curr2 = head2;
    while (curr1 != curr2) {
        curr1 = curr1->next;
        curr2 = curr2->next;
        if (!curr1) {
            curr1 = head1;
        }
        if (!curr2) {
            curr2 = head2;
        }
    }
    return curr1->data;
}
////////////////////////////////////////////////////////////////////////////////

SinglyLinkedListNode* mergeLists(SinglyLinkedListNode* head1,
                                 SinglyLinkedListNode* head2,
                                 int data)
{
    SinglyLinkedListNode* last1 = head1;
    SinglyLinkedListNode* last2 = head2;
    while (last1->next) {
        last1 = last1->next;
    }
    while (last2->next) {
        last2 = last2->next;
    }
    std::cout << "Last of list 1: " << last1->data << '\n';
    std::cout << "Last of list 2: " << last2->data << '\n';

    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    last1->next = node;
    last2->next = node;

    return node;
}

void displayNodes(SinglyLinkedListNode* head) {
    while (head) {
        std::cout << head->data << " -> ";
        head = head->next;
    }
    std::cout << "nullptr" << std::endl;
}

int main() {
    SinglyLinkedListNode* head1{};
    // a-b-c
    head1 = insertNodeAtTail(head1, 1);
    head1 = insertNodeAtTail(head1, 2);
    head1 = insertNodeAtTail(head1, 3);
    displayNodes(head1);

    SinglyLinkedListNode* head2{};
    // p-q
    head2 = insertNodeAtTail(head2, 4);
    head2 = insertNodeAtTail(head2, 5);
    displayNodes(head2);

    SinglyLinkedListNode* head{};
    // x
    head = mergeLists(head1, head2, 6);

    std::cout << "First merged: " << head->data << std::endl;

    // y-z
    head = insertNodeAtTail(head, 7);
    head = insertNodeAtTail(head, 8);

    // a-b-c-x-y-z
    displayNodes(head1);

    // p-q-x-y-z
    displayNodes(head2);

    std::cout << "Merged found: " << findMergeNode(head1, head2) << std::endl;

    return 0;
}
