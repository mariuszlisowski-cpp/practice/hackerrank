// reverse a singly linked list

// insert node at tail

#include <iostream>
#include <iterator>
#include <vector>

using namespace std;

class SinglyLinkedListNode {
public:
    int data;
    SinglyLinkedListNode* next;
    SinglyLinkedListNode(int node_data) {
        this->data = node_data;
        this->next = nullptr;
    }
};

SinglyLinkedListNode* insertNodeAtTail(SinglyLinkedListNode* head, int data) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    if (!head)
        head = node;
    else {
        SinglyLinkedListNode* last = head;
        while (last->next)
            last = last->next;
        last->next = node;
    }
    return head;
}

SinglyLinkedListNode* reverseNodes(SinglyLinkedListNode* head) {
    SinglyLinkedListNode* curr = head;
    SinglyLinkedListNode *prev{}, *next{};
    while (curr) {
        next = curr->next;
        curr->next = prev;
        prev = curr;
        curr = next;
    }
    return (head = prev);
}

void displayNodes(SinglyLinkedListNode* head) {
    while (head) {
        cout << head->data << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

int main() {
    SinglyLinkedListNode* head{};

    head = insertNodeAtTail(head, 10);
    head = insertNodeAtTail(head, 20);
    head = insertNodeAtTail(head, 30);
    displayNodes(head);

    head = reverseNodes(head);
    displayNodes(head);

    return 0;
}
