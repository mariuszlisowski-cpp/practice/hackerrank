// insert node at position

#include <iostream>

using namespace std;

class SinglyLinkedListNode {
public:
    int data;
    SinglyLinkedListNode* next;
    SinglyLinkedListNode(int node_data) {
        this->data = node_data;
        this->next = nullptr;
    }
};

SinglyLinkedListNode* insertNodeAtPosition(SinglyLinkedListNode* head, int data, int position) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    if (!head) {
        head = node;
    } else if (position == 0) {
        node->next = head;
        head = node;
    }
    // position NOT secured below
    else {
        int counter{};
        SinglyLinkedListNode* prev = head;
        SinglyLinkedListNode* next = head->next;
        while (counter < position - 1) {
            prev = prev->next;
            next = prev->next;
            ++counter;
        }
        prev->next = node;
        node->next = next;
    }
    return head;
}

void displayNodes(SinglyLinkedListNode* head) {
    while (head) {
        cout << head->data << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

int main() {
    SinglyLinkedListNode* head{};

    head = insertNodeAtPosition(head, 10, 0);
    head = insertNodeAtPosition(head, 20, 1);
    head = insertNodeAtPosition(head, 30, 0);
    head = insertNodeAtPosition(head, 40, 3);
    head = insertNodeAtPosition(head, 50, 2);
    displayNodes(head);

    return 0;
}
