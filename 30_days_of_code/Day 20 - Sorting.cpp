// Hackerrank
// 30 days of code
// Day 20: Sorting

#include <iostream>
#include <vector>

using namespace std;

int bubbleSortAscending(vector<int>&, const int &);

int main() {
    int n;
    cin >> n;
    vector<int> a(n);
    for(int a_i = 0; a_i < n; a_i++){
        cin >> a[a_i];
    }

    int numSwaps = bubbleSortAscending(a, n);

    cout << "Array is sorted in " << numSwaps << " swaps." << endl;
    cout << "First Element: " << a[0] << endl;
    cout << "Last Element: " << a[a.size() - 1] << endl;

    return 0;
}

int bubbleSortAscending(vector<int>& arr, const int &size) {
  int numberOfSwaps = 0;
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size - 1; j++) {
      // swap adjacent elements if they are in decreasing order
      if (arr[j] > arr[j + 1]) {
        swap(arr[j], arr[j + 1]);
        numberOfSwaps++;
      }
    }
    // if no elements were swapped during a traversal, array is sorted
    if (numberOfSwaps == 0)
      break;
  }
  return numberOfSwaps;
}
