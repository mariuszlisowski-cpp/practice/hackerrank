// Hackerrank
// 30 days of code
// Day 10: Binary Numbers

#include <iostream>

using namespace std;

string toBinary(int);

int main()
{
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    string binary = toBinary(n);

    int max = 0, count = 0;
    for (int i = 0; i < binary.size(); i++) {
        if (binary[i] == '0') {
        if (count > max )
            max = count;
        count = 0;
        }
        else {
            count++;
            if (i == binary.size() - 1)
                if (count > max )
                    max = count;
        }
    }

    cout << max << endl;

    return 0;
}

string toBinary(int n) {
    string s;
    while (n != 0) {
      s = (n % 2 == 0 ? "0" : "1") + s;
      n /= 2;
    }
    return s;
}
