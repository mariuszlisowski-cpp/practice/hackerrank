// Hackerrank
// 30 days of code
// Day 24: More Linked Lists

#include <cstddef>
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;	
class Node
{
    public:
        int data;
        Node *next;
        Node(int d){
            data=d;
            next=NULL;
        }
};
class Solution{
    public:

// ------------------------------ solution ------------------------------
          Node* removeDuplicates(Node *head)
          {
            if (head == NULL)
                return head;
            Node* start = head;
            while (start) {
                if (start->next == NULL)
                    return head; // end of list
                else if (start->data == start->next->data) {
                    Node* temp = start->next;
                    start->next = temp->next;
                    delete temp;
                    continue; // if more following duplicates
                }
            start = start->next; // no following duplicates
            }
          return head;
          }
// ----------------------------------------------------------------------

          Node* insert(Node *head,int data)
          {
               Node* p=new Node(data);
               if(head==NULL){
                   head=p;  

               }
               else if(head->next==NULL){
                   head->next=p;

               }
               else{
                   Node *start=head;
                   while(start->next!=NULL){
                       start=start->next;
                   }
                   start->next=p;   

               }
                    return head;
                
            
          }
          void display(Node *head)
          {
                  Node *start=head;
                    while(start)
                    {
                        cout<<start->data<<" ";
                        start=start->next;
                    }
           }
};
            
int main()
{
    Node* head=NULL;
    Solution mylist;
    int T,data;
    cin>>T;
    while(T-->0){
        cin>>data;
        head=mylist.insert(head,data);
    }	
    head=mylist.removeDuplicates(head);

    mylist.display(head);
        
}