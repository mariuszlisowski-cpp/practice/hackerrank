// Hackerrank
// 30 days of code
// day 11: 2D arrays

#include <iostream>
#include <vector>

using namespace std;

int main() {
  vector<vector<int>> arr(6);
  for (int i = 0; i < 6; i++) {
    arr[i].resize(6);
    for (int j = 0; j < 6; j++) {
      cin >> arr[i][j];
    }
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
  }

  int hg[4][4] = {0};
  int index, i = 0, j;
  vector<vector<int> >::const_iterator row;
  vector<int>::const_iterator col;
  for (row = arr.begin(); row != arr.end(); row++, i++) {
    index = row - arr.begin();
    j = 0;
    for (col = row->begin(); col != row->end()-2; col++, j++) {
      hg[i][j] += *col + *(col + 1) + *(col + 2);
      if (index >= 1 && index <= 4)
        hg[i-1][j] += *(col + 1);
      if (index >= 2)
        hg[i-2][j] += *col + *(col + 1) + *(col + 2);
     }
  }

  int temp, max = -64;
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      temp = hg[i][j];
      //cout << temp << endl;
      if ( temp > max)
        max = temp;
    }
  }

  cout << max << endl;

  return 0;
}

/* sample input
vector<vector<int>> arr {
    { 1, 1, 1, 0, 0, 0 },
    { 0, 1, 0, 0, 0, 0 },
    { 1, 1, 1, 0, 0, 0 },
    { 0, 0, 2, 4, 4, 0 },
    { 0, 0, 0, 2, 0, 0 },
    { 0, 0, 1, 2, 4, 0 }
};
/*

/* sample input
vector<vector<int>> arr {
    { -1, -1,  0, -9, -2, -2 },
    { -2, -1, -6, -8, -2, -5 },
    { -1, -1, -1, -2, -3, -4 },
    { -1, -9, -2, -9, -9, -9 },
    { -7, -3, -3, -9, -9, -9 },
    { -1, -3, -1, -9, -9, -9 }
  };
*/
