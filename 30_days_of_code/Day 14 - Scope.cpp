// Hackerrank
// 30 days of code
// Day 14: Scope

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

class Difference {
    private:
    vector<int> elements;

    public:
    int maximumDifference;

    // ------------------------------ solution ------------------------------ 
    Difference(vector<int> v):elements(v) {}
    void computeDifference() {
        int absolute, max = 0;
        vector<int>::const_iterator it;
        vector<int>::const_iterator itt;
        for (it = elements.begin(); it != elements.end(); it++) {
            absolute = 0;
            for (itt = elements.begin(); itt != elements.end(); itt++) {
                absolute = abs(*it - *itt);
                if (absolute > max)
                  max = absolute;
            }
        }
        maximumDifference = max;
    }
    // ----------------------------------------------------------------------

}; // End of Difference class

int main() {
    int N;
    cin >> N;

    vector<int> a;

    for (int i = 0; i < N; i++) {
        int e;
        cin >> e;

        a.push_back(e);
    }

    Difference d(a);

    d.computeDifference();

    cout << d.maximumDifference;

    return 0;
}
