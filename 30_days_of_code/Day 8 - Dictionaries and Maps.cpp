// Hackerrank
// 30 days of code
// Day 8: Dictionaries and Maps

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>

using namespace std;

int main() {
  string s;
  vector<string> names;
  map<string, int> phonebook;

  int t, n;
  cin >> t;
  while (t--) {
    cin >> s >> n;
    phonebook[s] = n;
  }

  while (cin >> s) {
    names.push_back(s);
  }

  vector<string>::const_iterator it;
  for (it = names.begin(); it != names.end(); it++) {
    if (phonebook.find(*it) != phonebook.end())
      cout << *it << "=" << phonebook[*it] << endl;
    else
      cout << "Not found" << endl;
  }

  return 0;
}
