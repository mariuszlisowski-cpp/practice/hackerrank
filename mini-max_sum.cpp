#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

void miniMaxSum(std::vector<int> arr) {
    std::sort(arr.begin(), arr.end());
    auto min = std::accumulate(arr.begin(), arr.end() - 1, static_cast<long long>(0));
    auto max = std::accumulate(arr.begin() + 1, arr.end(), static_cast<long long>(0));

    std::cout << min << ' ' << max << std::endl;
}


int main() {
    miniMaxSum(std::vector<int>{1, 3, 5, 7, 9}); // min:0 16, max: 24
    miniMaxSum(std::vector<int>{140537896, 243908675, 670291834, 923018467, 520718469}); // min: 1575456874
                                                                                         // max: 2357937445

    return 0;
}
