/* Time in AM/PM format converter to military (24-hour) time. */
#include <algorithm>
#include <iostream>
#include <map>
#include <string>

std::string time_conversion_from_military(std::string s) {
    std::map<std::string, std::string> hours{
        {"01", "13"},   // for PM
        {"02", "14"},   // ...
        {"03", "15"},
        {"04", "16"},
        {"05", "17"},
        {"06", "18"},
        {"07", "19"},
        {"08", "20"},
        {"09", "21"},
        {"10", "22"},
        {"11", "23"},
        {"12", "00"},   // for AM only
    };
    std::string hour;
    auto colon{s.find(":")};
    if (colon != std::string::npos) {
        hour = s.substr(0, colon);
    }

    auto period{s.substr(s.size() - 2, 2)};
    auto h_it = hours.find(hour);
    if ((period == "PM" && hour != "12") ||     // all PM except 12
        (period == "AM" && hour == "12"))       // only AM 12
    {
        if (h_it != hours.end()) {
            s.replace(0, colon, h_it->second);
        }
    }

    return s.erase(s.size() - 2, 2);
}

int main() {
    std::string input{"12:40:22AM"}; // 00:40:22
    // std::string input{"07:05:45PM"}; // 19:05:45
    // std::string input{"07:05:45AM"}; // 07:05:45 (no change)
    // std::string input{"12:45:54PM"}; // 12:45:54 (no change)
    
    std::cout << time_conversion_from_military(input);

    return 0;
}
